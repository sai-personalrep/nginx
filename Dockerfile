FROM nginx:latest
RUN apt-get update
RUN mkdir -p /var/www/
RUN chown -R www-data:www-data /var/www/
RUN chmod -R 775 /var/www/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./ssksalesapi.conf /etc/nginx/conf.d/
expose 80
expose 443
expose 8000
